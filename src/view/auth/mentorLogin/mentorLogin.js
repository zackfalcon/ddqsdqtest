import React from 'react';
import { Row } from 'antd';
import logoShareXp from '../../../assets/logo.png';
import { Link, withRouter } from 'react-router-dom';
import GoogleConnect from '../../../components/googleConnect';
import LinkedinConnect from '../../../components/linkedinConnect';
import Styles from './mentorLogin.less';

class MentorLogin extends React.PureComponent {
    render() {
        return (
            <div>
                <div className={Styles.loginRow}>
                    <div className={Styles.leftPanel}>
                        <div className={Styles.panelContent}>
                            <Link to="/home"><img src={logoShareXp} alt="logo-sharexp" /></Link>
                            <div className={Styles.formDivider}>
                                <h2 className={Styles.heading}>Heureux de vous revoir !</h2>
                                <span className={Styles.textSpan}>Choisis la méthode que vous préfèrez pour continuer :</span>
                                <Row>
                                    <Link to="/auth/mentorProfile"><LinkedinConnect /></Link>
                                    <GoogleConnect onClick={console.log("Google Setup done.")} />
                                </Row>
                            </div>
                            <Row className={Styles.panelFooter}>
                                <Row>
                                    <span className={Styles.footerText}>Pas encore de compte ?</span>
                                    <Link to="/auth/register"><span className={Styles.footerLink}>Inscrivez-vous !</span></Link>
                                </Row>
                                <Row>
                                    <span className={Styles.footerText}>Vous êtes un professionel ?</span>
                                    <Link to="/auth/mentorLogin"><span className={Styles.footerLink}>Par ici !</span></Link>
                                </Row>
                            </Row>
                        </div>
                    </div>
                    <div className={Styles.rightPanel}>
                        <div className={Styles.panelContent}>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default (withRouter(MentorLogin))
