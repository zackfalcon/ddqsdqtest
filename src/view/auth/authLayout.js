import React from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import logoSwapcard from '../../assets/logo.png';
import Styles from './authLayout.less';
import Header from '../main/header';

class AuthLayout extends React.PureComponent {

    render() {
        return (
          <div>
            <Header />
            <div className={Styles.form}>
                <div className={Styles.logo}>
                    <img alt="logo_epitech" src={logoSwapcard} />
                </div>
                <Link to="/auth/login">
                    <Button type="primary" size="large"
                        className={Styles.btnConnexion}>Connexion</Button>
                </Link>
                <Link to="/auth/register">
                    <Button type="primary"  size="large"
                    className={Styles.btnInscription}>Inscription</Button>
                </Link>
                <span className={Styles.textAuth}>ShareXP Experience - Copyright 2019</span>
                <Button type="primary" size="large"
                                    className={Styles.btnInscription} onClick={() => this.props.history.push('/home')}>Retour à l'acceuil</Button>

            </div>
          </div>
        );
    }
}



export default AuthLayout;
