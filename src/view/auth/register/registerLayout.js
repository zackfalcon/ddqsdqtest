import React from 'react';
import { Form, Input, Button, Row, Alert } from 'antd';
import logoShareXp from '../../../assets/logo.png';
import { Link, withRouter } from 'react-router-dom';
import { graphql } from "react-apollo";
import gql from "graphql-tag";
import Styles from './register.less';

const FormItem = Form.Item;

const isEmpty = (value) => {
    return (
        value === undefined ||
        value === null ||
        (typeof value === 'object' && Object.keys(value).length === 0) ||
        (typeof value === 'string' && value.trim().length === 0)
    );
}


class Register extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            email: '',
            username: '',
            name: '',
            password: '',
            password_confirm: '',
            errors_server: null,
            validate: null
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }



    onSubmit = async () => {
        let stop = false;
        this.setState({
            errors_server: null
        })
        if (isEmpty(this.state.name) || isEmpty(this.state.email) || isEmpty(this.state.username) || isEmpty(this.state.password) || isEmpty(this.state.password_confirm)) {
            this.setState({
                errors_server: "Des champs sont encore vide."
            })
            stop = true;
        }
        if (this.state.password !== this.state.password_confirm) {
            this.setState({
                errors_server: "Les deux mots de passe ne sont pas similaire"
            })
            stop = true;
        }
        if (!stop) {
            const { email, username, name, password } = this.state
            const response = await this.props.mutate({
                variables: { email, username, name, password }
            })
            const { bool, message } = response.data.signUp;
            console.log(bool);
            console.log(message);
            if (bool) {
                this.setState({
                    validate: "Inscription finalisé : Connectez vous dès maintenant !"
                })
            } else {
                this.setState({
                    errors_server: message
                })
            }
        }
    }


    render() {
        return (
            <div>
                <div className={Styles.loginRow}>
                    <div className={Styles.leftPanel}>
                        <div className={Styles.panelContent}>
                            <Link to="/home"><img src={logoShareXp} alt="logo-sharexp" /></Link>
                            <div className={Styles.formDivider}>
                                <h2 className={Styles.heading}>Que l'aventure commence !</h2>
                                {this.state.errors_server && (<Alert message={this.state.errors_server} type="error" className={Styles.errorAlert}></Alert>)}
                                {this.state.validate && (<Alert message={this.state.validate} type="success"></Alert>)}

                                <form onSubmit={this.handleSubmit}>
                                    <FormItem hasFeedback style={{ marginBottom: 5 }}>
                                        <span className={Styles.label}>Nom</span>
                                        <Input type="text" placeholder="Nom" name="name"
                                            onChange={this.handleInputChange} value={this.state.name}
                                        />
                                    </FormItem>
                                    <FormItem hasFeedback style={{ marginBottom: 5 }}>
                                        <span className={Styles.label}>nom d'utilisateur</span>
                                        <Input type="text" placeholder="Nom d'utilisateur" name="username"
                                            onChange={this.handleInputChange} value={this.state.username} />
                                    </FormItem>
                                    <FormItem hasFeedback style={{ marginBottom: 5 }}>
                                        <span className={Styles.label}>E-MAIL</span>
                                        <Input type="text" placeholder="Email" name="email"
                                            onChange={this.handleInputChange} value={this.state.email} />
                                    </FormItem>
                                    <FormItem hasFeedback style={{ marginBottom: 5 }}>
                                        <span className={Styles.label}>MOT DE PASSE</span>
                                        <Input type="password" placeholder="Entrez votre mot de passe" name="password"
                                            onChange={this.handleInputChange} value={this.state.password} />
                                    </FormItem>
                                    <FormItem hasFeedback style={{ marginBottom: 15 }}>
                                        <span className={Styles.label}>CONFIRMER LE MOT DE PASSE</span>
                                        <Input type="password" placeholder="Confirmer le mot de passe" name="password_confirm"
                                            onChange={this.handleInputChange} value={this.state.password_confirm} />
                                    </FormItem>
                                    <Row>
                                        <Button type="primary" className={Styles.btnSharexp} onClick={this.onSubmit}>S'inscrire</Button>
                                    </Row>
                                </form>
                            </div>

                            <Row className={Styles.panelFooter}>
                                <Row>
                                    <span className={Styles.footerText}>Compte déjà créé ?</span>
                                    <Link to="/auth/login"><span className={Styles.footerLink}>Connectez-vous !</span></Link>
                                </Row>
                                <Row>
                                    <span className={Styles.footerText}>Vous êtes un professionel ?</span>
                                    <Link to="/auth/mentorLogin"><span className={Styles.footerLink}>Par ici !</span></Link>
                                </Row>
                            </Row>

                        </div>
                    </div>
                    <div className={Styles.rightPanel}>
                        <div className={Styles.panelContent}>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const query = graphql(
    gql`
      mutation signUp($email: String!, $username: String!, $name: String!, $password: String!) {
        signUp(email: $email, username: $username, name: $name, password: $password) {
            bool
            token
            message
        }
      }
    `
);


const RegisterRouter = withRouter(Register);
const RegisterApollo = query(RegisterRouter);

export default RegisterApollo
