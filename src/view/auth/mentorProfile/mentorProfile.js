import React, { Component } from 'react'
import Styles from './mentorProfile.less';
import { Layout, Menu, Form, Input, Row, Button } from 'antd';
import { Link } from 'react-router-dom';
import logo from '../../../assets/logo.png';

const FormItem = Form.Item;

class MentorProfile extends Component {

    constructor() {
        super();
        this.state = {
            email: '',
            username: '',
            name: '',
            password: '',
            password_confirm: '',
            errors_server: null,
            validate: null
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    onSubmit() {

    }

    render() {
        return (
            <div style={{ background: '#f4f5f6', minHeight: '100vh' }}>
                <Layout className={Styles.headerBlock} style={{ background: '#f4f5f6' }}>
                    <Menu
                        theme="light"
                        mode="horizontal"
                        selectedKeys={"mentor"}
                        className={Styles.menuBlock}>
                        <Link to="/home"><img src={logo} alt="logo" className={Styles.headerLogo} /></Link>
                        <Menu.Item key="mentor" className={Styles.becomeMentor} >Déconnexion</Menu.Item>
                        <Menu.Item key="mentor" className={Styles.becomeDispo} style={{ color: 'green !important' }}>DISPONIBLE</Menu.Item>
                        <Menu.Item key="help" className={Styles.becomeGray}>MODIFIER MON PROFIL</Menu.Item>
                        <Menu.Item key="help" className={Styles.becomeGray}>Demande de chat</Menu.Item>
                    </Menu>
                </Layout>
                <center>  <img src="https://image.noelshack.com/fichiers/2019/08/3/1550666283-telechargement.png" style={{ marginTop: 100, textAlign: 'center', width: 'auto' }} alt="" />
                </center>
            </div>
        )
    }
}

export default MentorProfile;