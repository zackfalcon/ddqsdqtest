import React from 'react';
import { Form, Input, Button, Row, Alert } from 'antd';
import logoShareXp from '../../../assets/logo.png';
import { Link, withRouter } from 'react-router-dom';
import { graphql } from "react-apollo";
import gql from "graphql-tag";
import Styles from './login.less';

const FormItem = Form.Item;

const isEmpty = (value) => {
    return (
        value === undefined ||
        value === null ||
        (typeof value === 'object' && Object.keys(value).length === 0) ||
        (typeof value === 'string' && value.trim().length === 0)
    );
}

class Login extends React.PureComponent {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            errors_server: null,
            validate: null
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit = async () => {
        let stop = false;
        this.setState({
            errors_server: null
        })
        if (isEmpty(this.state.username) || isEmpty(this.state.password)) {
            this.setState({
                errors_server: "Des champs sont encore vide."
            })
            stop = true;
        }
        if (!stop) {
            const { username, password } = this.state
            const response = await this.props.mutate({
                variables: { username, password }
            })
            const { bool, token, message } = response.data.login;
            if (bool) {
                localStorage.setItem('jwtToken', token);
                localStorage.setItem('userName', username);
                console.log(localStorage.getItem('jwtToken'));
                console.log(localStorage.getItem('userName'));
                this.props.history.push('/');
            } else {
                this.setState({
                    errors_server: message
                })
            }
        }
    }

    render() {
        return (
            <div>
                <div className={Styles.loginRow}>
                    <div className={Styles.leftPanel}>
                        <div className={Styles.panelContent}>
                            <Link to="/home"><img src={logoShareXp} alt="logo-sharexp" /></Link>
                            <div className={Styles.formDivider}>
                                <h2 className={Styles.heading}>Très content de te revoir !</h2>
                                {this.state.errors_server && (<Alert message={this.state.errors_server} type="error" className={Styles.errorAlert}></Alert>)}

                                <form onSubmit={this.handleSubmit}>
                                    <FormItem hasFeedback style={{ marginBottom: 5 }}>
                                        <span className={Styles.label}>Nom d'utilisateur</span>
                                        <Input type="text" placeholder="Nom d'utilisateur" name="username"
                                            onChange={this.handleInputChange} value={this.state.username}
                                        />
                                    </FormItem>
                                    <FormItem hasFeedback style={{ marginBottom: 15 }}>
                                        <span className={Styles.label}>MOT DE PASSE</span>
                                        <Input type="password" placeholder="Entrez votre mot de passe" name="password"
                                            onChange={this.handleInputChange} value={this.state.password} />
                                    </FormItem>
                                    <Row>
                                        <Button type="primary" className={Styles.btnSharexp} onClick={this.onSubmit}>Se connecter</Button>
                                        <Link to="/auth/register"><span className={Styles.passwordLost}>Mot de passe oublié ?</span></Link>
                                    </Row>
                                </form>
                            </div>

                            <Row className={Styles.panelFooter}>
                                <Row>
                                    <span className={Styles.footerText}>Pas encore de compte ?</span>
                                    <Link to="/auth/register"><span className={Styles.footerLink}>Inscrivez-vous !</span></Link>
                                </Row>
                                <Row>
                                    <span className={Styles.footerText}>Vous êtes un professionel ?</span>
                                    <Link to="/auth/mentorLogin"><span className={Styles.footerLink}>Par ici !</span></Link>
                                </Row>
                            </Row>

                        </div>
                    </div>
                    <div className={Styles.rightPanel}>
                        <div className={Styles.panelContent}>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const query = graphql(
    gql`
      mutation login($username: String!, $password: String!) {
        login(username: $username, password: $password) {
            bool
            token
            message
        }
      }
    `
);

const LoginRouter = withRouter(Login);
const LoginApollo = query(LoginRouter);

export default (withRouter(LoginApollo))
