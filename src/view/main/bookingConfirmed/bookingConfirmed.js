import React from 'react'
import { withRouter } from 'react-router-dom'
import { Layout, Row, Col, Icon, Button } from 'antd'
import Header from '../header'
import Styles from './bookingConfirmed.less'

class BookingConfirmed extends React.PureComponent {
	constructor(props) {
		super(props);
		this.goHome = this.goHome.bind(this);
	}

	goHome() {
		this.props.history.push({
			pathname: '/home'
		})
	}

	render() {
		return (
			<Layout style={{ background: 'white' }}>
				<Header />
				<Row className={Styles.blockGridList}>
					<Col span={15} offset={5}>
						<Row gutter={20} className={Styles.gridListByUser}>
							<Col span={6} className={Styles.imgUser}><img alt="" src={this.props.location.state.image} width="230" /></Col>
							<Col span={18} className={Styles.userInformation}>
								<h1 className={Styles.userName}><span className={Styles.colorized}>{this.props.location.state.name}</span></h1>
								<hr style={{ opacity: 0.3 }} />
								<h2 className={Styles.userAvailable}><Icon type="check-circle" /> Demande de rendez-vous prise en compte !</h2>
								<h2 className={Styles.userTime}><Icon type="clock-circle" /> Le {this.props.location.state.date} à {this.props.location.state.hour}</h2>
								<h2 className={Styles.userTime}>Ce mentor vous contactera ultérieurement via notre chat !</h2>
								<Button type="primary" className={Styles.buttonBooking} onClick={this.goHome}>
									<p>Retour à l'accueil</p>
								</Button>
							</Col>
						</Row>
					</Col>
				</Row>
			</Layout>
		);
	}
}

export default withRouter(BookingConfirmed);