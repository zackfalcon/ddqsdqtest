import React from 'react';

import { withRouter } from 'react-router-dom';
import { Layout, Row, Col } from 'antd';
import { Steps, Popover } from 'antd';

import ProfileHeader from '../profileHeader';
import Styles from './profileMentor.less';
import Comment from '../../comment';

const data = {
    titleDesc: "Bien plus qu'un simple mot mon metier est épanouissant ! Viens le découvrir !",
    textDesc: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`,
    ecole: [{
        name: 'HEC Business School | Master',
        infoParcours: 'Obtention d\'un master master en commerce.',
        image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/HEC_Paris.svg/1200px-HEC_Paris.svg.png',
        date: '2016-2018',
    },
    {
        name: 'Skema Business School',
        infoParcours: 'Obtention d\'un diplome de commerce.',
        image: 'https://s3-eu-west-1.amazonaws.com/assets.atout-on-line.com/images/commerce/FichesEcoles/logo_skema.JPG',
        date: '2012-2015',
    },
    {
        name: 'Lycée Jules Ferry',
        infoParcours: 'Obtention du BAC.',
        image: null,
        date: '2009-2012',
    }
    ],
    professionel: [{
        name: 'Consultant chez InsideBoard',
        infoParcours: 'Occupation des comptes du personnels.',
        image: 'https://www.insideboard.com/wp-content/themes/insideboard/theme/images/logos/logo-insideboard_header.png',
        date: '2016-2018',
    },
    {
        name: 'Consultant chez Société Général',
        infoParcours: 'Occupaton du service finance.',
        image: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Soci%C3%A9t%C3%A9_G%C3%A9n%C3%A9rale.svg',
        date: '2015-2015',
    },
    {
        name: 'Vendeur à TheKooples',
        infoParcours: 'Mise en rayonnage.',
        image: 'https://logos-download.com/wp-content/uploads/2016/05/The_Kooples_logo.png',
        date: '2011-2012',
    },
    {
        name: 'Vendeur à LIDL',
        infoParcours: 'Obtention du BAC.',
        image: null,
        date: '2011-2011',
    }
    ],
    avis: [
        {
            name: 'Yaniss',
            message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
            rate: 4,
            profileImage: 'https://media.licdn.com/dms/image/C4E03AQFZjpu3fhDtSA/profile-displayphoto-shrink_800_800/0?e=1550102400&v=beta&t=Z-UVNlG5mCPSD-J6IbJZHIX-I8naPq_PuIapD7YMJpg'
        },
        {
            name: 'Paola',
            message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            rate: 3,
            profileImage: 'https://media.licdn.com/dms/image/C5103AQEp3MsPu7YOQw/profile-displayphoto-shrink_800_800/0?e=1550102400&v=beta&t=skotUfkFtkT699sKnP9dX3I5gLHieSLdyxl0WSSvdmw'
        },

        {
            name: 'Zack',
            message: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            rate: 5,
            profileImage: 'https://media.licdn.com/dms/image/C4D03AQFw-IghsaeZZg/profile-displayphoto-shrink_200_200/0?e=1550102400&v=beta&t=Z-gIWta4qW5TVzMMdewuhH7qT96FQjZbKlaoP7KN9zg'
        },

        {
            name: 'Matthieu',
            message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut .',
            rate: 3,
            profileImage: 'https://media.licdn.com/dms/image/C5603AQHZbhDSxcitTQ/profile-displayphoto-shrink_800_800/0?e=1550102400&v=beta&t=hgKDiOq2DkpwN5a7gGiHJV4uO_uYV04FBe9Ke94H_00'
        },
    ]
};
const Step = Steps.Step;

const schoolDot = (dot, { index }) => (
    <Popover content={<span>{data.ecole[index].date}</span>}>
        {dot}
    </Popover>
);

const proDot = (dot, { index }) => (
    <Popover content={<span>{data.professionel[index].date}</span>}>
        {dot}
    </Popover>
);


class ProfileMentor extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            bookingDone: false
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }
    parcoursInfo(link, name) {
        return (
            <div style={{ display: 'block' }}>
                {link !== null && <img alt={name} src={link} style={{ display: 'block' }} width="100" />}
                <b style={{ display: 'block' }}>{name}</b>
            </div>
        );
    }

    render() {
        return (
            <Layout style={{ background: 'white' }}>
                <ProfileHeader data={this.props.location.state.data} />
                    <Layout className={Styles.mentorBlockInfo}>
                        <Row className={Styles.blockInfo}>
                            <Col span={12} offset={6} className={Styles.blockDesc}>
                                <h1 className={Styles.titleDesc}>
                                    {data.titleDesc}
                                </h1>
                                <span className={Styles.textDesc}>
                                    Bonjour,<br /><br />
                                    {data.textDesc}
                                </span>
                            </Col>
                            <Col span={14} offset={5} className={Styles.blockParcours}>
                                <Row>
                                    <Col span={12}>
                                        <h1 className={Styles.titleParcours}>Parcours Scolaire</h1>
                                        <Steps direction="vertical" current={data.ecole.length} progressDot={schoolDot}>
                                            {data.ecole.map(data =>
                                                <Step title={this.parcoursInfo(data.image, data.name)} description={data.infoParcours} />)}
                                        </Steps>
                                    </Col>
                                    <Col span={12} style={{ transform: [{ rotate: '90deg' }] }}>
                                        <h1 className={Styles.titleParcours}>Parcours Professionel</h1>
                                        <Steps direction="vertical" current={data.professionel.length} progressDot={proDot}>
                                            {data.professionel.map(data =>
                                                <Step title={this.parcoursInfo(data.image, data.name)} description={data.infoParcours} />)}
                                        </Steps>
                                    </Col>
                                </Row>
                            </Col>
                            <Col span={12} offset={6} className={Styles.blockAvis}>
                                <h1 className={Styles.titleAvis}>Les avis sur {this.props.location.state.data.name}</h1>
                                {data.avis.map(data =>
                                    <Comment data={data} />
                                )}
                            </Col>
                        </Row>
                    </Layout>
            </Layout>
        );
    }
}

export default withRouter(ProfileMentor);