import React from 'react'

import { withRouter } from 'react-router-dom';
import { Layout, Row, Col, Rate, Button, Calendar, Icon, Alert } from 'antd';

import Styles from './profileHeader.less';
import Header from '../../header';

import moment from 'moment';
import TimePickerPanel from 'rc-time-picker/lib/Panel';

class ProfileHeader extends React.PureComponent {

	constructor(props) {
		super(props);
		this.confirmBooking = this.confirmBooking.bind(this);
	}
	state = {
		isAvailable: true,
		error: null
	}
	onPanelChange(value, mode) {
		console.log(value, mode);
	}


	disableDate(current) {
		if (!current) {
		  // allow empty select
		  return false;
		}
		var current_date = (current.format('L')).split('/');
		var day = current_date[1];
		var month =  current_date[0];

		const date = moment();
		date.hour(0);
		date.minute(0);
		date.second(0);
		if (current.valueOf() < date.valueOf()) { // can not select days before today
			if (((day === 24 || day === 31) && month === 12) || (day === 1 && month === 1))
				return false;
			return true;
		} else {
			if (((day === 24 || day === 31) && month === 12) || (day === 1 && month === 1))
				return true;
			return false;
		}
	}

	timePickerElement = <TimePickerPanel defaultValue={moment('00:00:00', 'HH:mm:ss')} />;


	confirmBooking() {
		if (!localStorage.getItem('jwtToken')) {
			this.setState({
				error: 'Vous devez être connecté pour pouvoir reserver un mentor.'
			})
		} else {
			this.props.history.push({
				pathname: '/bookingConfirmed',
				state: {
					date: '14 Novembre',
					hour: '14h30',
					image: this.props.data.image,
					name: this.props.data.name
				}
			})
		}
	}

	render() {
		return (
			<Layout style={{ background: 'white' }}>
				<Header />
				<Row className={Styles.blockGridList}>

					<Col span={15} offset={5}>
					{this.state.error && (<Alert message={this.state.error} type="error" className={Styles.errorAlert}></Alert>)}

						<Row gutter={20} className={Styles.gridListByUser}>
							<Col span={6} className={Styles.imgUser}><img alt="" src={this.props.data.image} width="250" /></Col>
							<Col span={12} className={Styles.userInformation}>
								<h1 className={Styles.userName}><span className={Styles.colorized}>{this.props.data.name},</span> {this.props.data.age} ans</h1>

								<h2 className={Styles.userJob}><span className={Styles.colorized}>{this.props.data.job},</span> à {this.props.data.location}</h2>
								<h2 className={Styles.userCompany}>SOCIETE: <img className={Styles.imgJob} alt="" src={this.props.data.company} width="120px" /></h2>
								<div style={{ display: 'block' }}>
									<Rate style={{ color: '#00C9FF', display: 'inline-block', marginBottom: 10 }} disabled defaultValue={this.props.data.note} />
									<span style={{ display: 'inline-block', fontWeight: 'bold', color: '#00C9FF' }}>({this.props.data.avis} avis)</span>
								</div>
								<hr style={{ opacity: 0.3 }} />

								{this.state.isAvailable === false && <h2 className={Styles.userAvailableNot}><Icon type="stop" /> Non Disponible avant le 14 Décembre.</h2>}
								{this.state.isAvailable && <h2 className={Styles.userAvailable}><Icon type="check-circle" /> Disponible.</h2>}
								<h2 className={Styles.userTime}><Icon type="clock-circle" /> Temps de réponse moyens : 1h à 2h</h2>
							</Col>
							<Col span={5} className={Styles.profileBooking}>
								<Button type="primary" className={Styles.buttonBooking} onClick={this.confirmBooking}>
									<div className={Styles.textBtn}>
										<p>Réserver ce mentor</p>
									</div>
								</Button>
								<div style={{ marginTop: 20, width: 280, border: '2px solid #00C9FF', borderRadius: 4 }}>
									<Calendar
										disabledDate={this.disableDate}
										fullscreen={false}
										onPanelChange={this.onPanelChange}
										timePicker={this.state.showTime ? this.timePickerElement : null}
									/>
								</div>
							</Col>
						</Row>
					</Col>
				</Row>
			</Layout>
		);
	}

}

export default withRouter(ProfileHeader);
