import React from 'react';
import { Layout, Row, Col, Button } from 'antd';
import Styles from './page2.less';
import Media from 'react-media';
import QueueAnim from 'rc-queue-anim';
import ScrollOverPack from 'rc-scroll-anim/lib/ScrollOverPack';

import { withRouter } from 'react-router-dom';
import SearchMentor from '../../../../assets/search.png';
import BookMentor from '../../../../assets/book.png';
import MeetMentor from '../../../../assets/meet.png';

const mentor = [
    {
        image: 'https://cdn.welcometothejungle.co/uploads/video/image/4031/154227/small_galeries-lafayette_video_j2OawgJ.JPG',
        name: 'Clarisse',
        age: 22,
    },
    {
        image: 'https://cdn.welcometothejungle.co/uploads/video/image/3978/154227/small_galeries-lafayette_video_Zkd0M1w.JPG',
        name: 'Arnaud',
        age: 35,
    },
    {
        image: 'https://cdn.welcometothejungle.co/uploads/video/image/1643/154228/small_galeries-lafayette_video_VQg2AMx.jpg',
        name: 'Alisson',
        age: 42,
    },
    {
        image: 'https://cdn.welcometothejungle.co/uploads/video/image/4339/154227/small_galeries-lafayette_video_bLaAa6l.JPG',
        name: 'Anatol',
        age: 36,
    },
]


const student = [
    {
        image: 'https://cdn.welcometothejungle.co/uploads/video/image/4154/154227/small_galeries-lafayette_video_W4x0.jpg',
        name: 'Camille',
        age: 30,
    },
    {
        image: 'https://cdn.welcometothejungle.co/uploads/video/image/9191/154227/small_hi-botfuel-io_video_NYG5PqL.jpg',
        name: 'Zora',
        age: 22,
    },
    {
        image: 'https://cdn.welcometothejungle.co/uploads/video/image/9310/154227/small_hi-botfuel-io_video_R0Q8X6p.jpg',
        name: 'Kevin',
        age: 24,
    },
    {
        image: 'https://cdn.welcometothejungle.co/uploads/video/image/9419/154227/small_hi-botfuel-io_video_22Y054r.jpg',
        name: 'Xavier',
        age: 40,
    },
]

class Page2 extends React.PureComponent {
    render() {
        return (
            <Layout className={Styles.wrapThirdPart}>
                <div style={{ textAlign: 'center', minHeight: "364px" }}>
                    <Row>
                        <Col span={12} offset={6}><h1 className={Styles.titleThird}>Comment ça marche ?</h1></Col>
                    </Row>
                    <Row className={Styles.stepBlock}>
                    <ScrollOverPack playScale="0.1">
                        <QueueAnim interval="200">
                        <Col span={8} key="1">
                            <img alt="search_mentor" src={SearchMentor} className={Styles.icon}/>
                            <h2 className={Styles.iconTitle}>Rechercher un mentor</h2>
                        </Col>
                        <Col span={8} key="2">
                            <img alt="search_mentor" src={BookMentor} className={Styles.icon}/>
                            <h2 className={Styles.iconTitle}>Prenez un rendez-vous</h2></Col>
                        <Col span={8} key="3">
                            <img alt="search_mentor" src={MeetMentor} className={Styles.icon}/>
                            <h2 className={Styles.iconTitle}>Rencontrez votre mentor</h2></Col>
                        </QueueAnim>
                    </ScrollOverPack>
                    </Row>
                </div>

                

                <div style={{ textAlign: 'center', margin: 30 }}>
                {/* <List
                grid={{
                    gutter: 1, xs: 2, sm: 3, md: 4, lg: 4, xl: 6, xxl: 3,
                  }}
                  dataSource={mentor}
                  renderItem={data => (
                    <List.Item>
                        {
                            data.age > 28 ? 
                                <><img className={Styles.listImage} alt={data.name} src={data.image} /></> :
                                <><img className={Styles.listImage} alt={data.name} src={data.image} /></>
                        }
                    </List.Item>
                  )}
                >
                </List> */}

                    <Row type="flex" justify="space-around" align="middle">
                        {mentor.map(data => {
                            if (data.age > 28) {
                                return (<Col span={4} key={data.name} className={Styles.userItem}>
                                    <img className={Styles.userSquare} alt={data.name} src={data.image} />
                                </Col>)
                            } else {
                                return (<Col span={4} key={data.name} className={Styles.userItemOld}>
                                    <img className={Styles.userSquare} alt={data.name} src={data.image} />
                                </Col>)
                            }
                            })}
                    </Row>
                    <Row type="flex" justify="center" align="top">
                        {student.map(data => {
                            if (data.age > 28) {
                                return (<Col span={4} key={data.name} className={Styles.userItem}>
                                    <img className={Styles.userSquare} alt={data.name} src={data.image} />
                                </Col>)
                            } else {
                                return (<Col span={4} key={data.name} className={Styles.userItemOld}>
                                    <img className={Styles.userSquare} alt={data.name} src={data.image} />
                                </Col>)
                            }
                        })}
                    </Row>

                </div>

                <div style={{ textAlign: 'center', margin: 60 }}>
                    <Row>
                    <Media query={{ minWidth: 750 }}>{
                    matches => matches ? (
                        <><Col span={10} offset={2}><h1 className={Styles.titleThird}>Plus de 1000 Mentors disponible !</h1></Col>
                        <Col span={6} offset={2}><Button type="primary" className={Styles.btnMentor}>Commencer l'aventure</Button></Col></>
                    ) : (
                        <><Row span={10} offset={2}><h1 className={Styles.titleThird}>Plus de 1000 Mentors disponible !</h1></Row>
                        <Row span={6} offset={2}><Button type="primary" className={Styles.btnMentor}>Commencer l'aventure</Button></Row></>
                    )}
                    </Media>
                    </Row>
                </div>
            </Layout>
        )
    }
}

export default withRouter(Page2)