import React from 'react';
import Media from 'react-media';
import { Layout, Row, Col, Carousel } from 'antd';
import Styles from './page1.less';

const data = [
    {
        name: "Cassandre",
        job: "Acheteuse Accessoire",
        age: 23,
        img: "https://cdn.welcometothejungle.co/uploads/video/image/4088/154227/small_galeries-lafayette_video_4l62LOK.JPG",
        jobText: "Vendeuse à Gallerie Lafayette",
        descText: "Vendeuse passionée et extravertie"
    },
    {
        name: "Steven",
        job: "Etudiant en commerce de vente",
        age: 20,
        img: "https://cdn.welcometothejungle.co/uploads/video/image/3869/154227/small_galeries-lafayette_video_NZGJaAP.JPG",
        jobText: "Etudiant en école de commerce",
        descText: "De curieux à concentré sur son objectif"
    },
    {
        name: "Paola",
        job: "Acheteuse Pret à porter",
        age: 21,
        img: "https://cdn.welcometothejungle.co/uploads/video/image/3903/154227/small_galeries-lafayette_video_qD9J.jpg",
        jobText: "Chef de projet à TheKooples",
        descText: "Entreprendre est la clef de son travail"
    },
]

class Page2 extends React.PureComponent {
    render() {
        const items = data.map(item => { return (<>
                <img alt={item.name + " - " + item.job} src={item.img} style={{backgroundImage: 'black'}} className={Styles.carouselImg}/>
                <h1 className={Styles.feedBackText}>{item.name}, {item.age} ans<br />
                    <div className={Styles.jobText}>{item.jobText}</div>
                    <div className={Styles.descText}>{item.descText}</div>
                </h1></>
            )}
        );
        return (
            <Layout className={Styles.wrapSecondPart}>
                <Media query={{ maxWidth: 900 }}>{
                    matches => matches ? (
                        <Carousel autoplay>
                        {items.map(item => { return (
                            <div className={Styles.feedBackItem}>
                                {item}
                            </div>
                        )})}
                        </Carousel>
                    ) : (
                        <Row className={Styles.feedbackBlock}>
                            {items.map(item => { return (
                                <Col className={Styles.feedBackItem} span={8}>
                                    {item}
                                </Col>
                            )})}
                        </Row>
                    )}
                </Media>
            </Layout>
        )
    }
}

export default Page2