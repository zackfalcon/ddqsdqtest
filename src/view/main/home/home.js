import React from 'react';
import { withRouter } from 'react-router-dom';
import Page1 from './page1';
import Page2 from './page2';

class Home extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      onSearchMentor: false,
      job: '',
    };
  }

  render() {
    return (
        <div>
            <Page1/>
            <Page2/>
        </div>
        );
  }
}

export default withRouter(Home);
