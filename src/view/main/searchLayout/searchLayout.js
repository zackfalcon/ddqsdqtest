import React from 'react';
import { withRouter } from 'react-router-dom';
import { Layout, Input, Row, Col, Button, Alert, Icon } from 'antd';
import Header from '../header';

import Styles from './searchLayout.less';

const isEmpty = (value) => {
	return (
		value === undefined ||
		value === null ||
		(typeof value === 'object' && Object.keys(value).length === 0) ||
		(typeof value === 'string' && value.trim().length === 0)
	);
}

class SearchLayout extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			job: '',
			location: '',
			error: null,
			onSearch: false
		};
		this.onSearchMentor = this.onSearchMentor.bind(this);
		this.props.history.listen((location, action) => {
			if (location.pathname === '/home') {
				this.setState({
					onSearch: false
				})
			}
		})
	}

	onChangeJob = (e) => {
		this.setState({ job: e.target.value });
	}
	onChangeLocation = (e) => {
		this.setState({ location: e.target.value });
	}

	onSearchMentor() {
		this.setState({
			error: null,
		})
		if (isEmpty(this.state.job)) {
			this.setState({
				error: 'Vous n\'avez pas rentré de métier dans votre recherche.'
			})
		} else if (isEmpty(this.state.location)) {
			this.setState({
				error: 'Vous n\'avez pas rentré de location dans votre recherche.'
			})
		} else {
			this.setState({
				onSearch: true
			})
			this.props.history.push({
				pathname: '/search',
				state: {
					job: this.state.job,
					location: this.state.location
				}
			})
		}

	}

	render() {
		return (
			<div>
				<Header/>
				<Layout className={Styles.wrapHeader}>
					<div className={Styles.searchBlock}>
						{this.state.onSearch === false && <h1 className={Styles.titleHeader}>Trouvez le mentor qu'il vous faut !</h1>}
						{this.state.onSearch === true &&
							<h1 className={Styles.titleHeader}>
								9 <span className={Styles.colorizedText}>{this.state.job.charAt(0).toUpperCase() + this.state.job.slice(1)}</span>
								<br />aux alentours de <span className={Styles.colorizedText}>{this.state.location.charAt(0).toUpperCase() + this.state.location.slice(1)}</span>  disponible.</h1>
						}
					</div>
					{this.state.error && (<Alert message={this.state.error} type="error" className={Styles.errorAlert}></Alert>)}
					<Row>
						<Col span={12}>
							<Input
								placeholder="Métier, (Ingenieur, Commercial...)"
								onChange={this.onChangeJob}
								value={this.state.job}
								className={Styles.inputPage} />
						</Col>
						<Col span={12}>
							<Input
								placeholder="Emplacement (Paris, Marseille...)"
								value={this.state.location}
								onChange={this.onChangeLocation}
								className={Styles.inputPage} />
						</Col>
					</Row>
					<Button onClick={this.onSearchMentor} className={Styles.btnSearch} type="primary" size='large'><Icon type="search"/>Rechercher</Button>
				</Layout>
			</div>
		)
	}
}

export default withRouter(SearchLayout)
