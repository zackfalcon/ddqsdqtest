import React from 'react';

import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { Layout } from 'antd';
import SearchLayout from './searchLayout';
import Home from './home';
import SearchMentor from './searchMentor';
import Footer from './footer';

class MainLayout extends React.PureComponent {
  render() {
    return (
      <Layout style={{ minHeight: '100vh', background: 'white' }}>
      <SearchLayout/>
        <Switch>
          <Route exact path="/home" component={Home} />
          <Route exact path="/search" component={SearchMentor} />
          <Route exact path="/" render={() => <Redirect to="/home" />} />
        </Switch>
        <Footer/>
      </Layout>);
  }
}

export default withRouter(MainLayout);
