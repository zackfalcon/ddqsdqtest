import React from 'react';

import { withRouter } from 'react-router-dom';
import { Layout } from 'antd';
import { Row, Col, Rate } from 'antd';
import Styles from './comment.less'

class Comment extends React.PureComponent {
    render() {
        return (
            <div>
                <Layout style={{ background: '#f7f7f7' }}>
                    <Row className={Styles.commentBlock}>
                        <Col span={6}>
                            <img className={Styles.commentUserImg} alt={this.props.data.name} src={this.props.data.profileImage} width="250" />
                            <span className={Styles.commentName}>{this.props.data.name}</span>
                            <Rate style={{ color: '#00C9FF', display: 'inline-block', marginBottom: 10 }} disabled defaultValue={this.props.data.rate} />

                     </Col>
                     <Col span={18}>

                     <span className={Styles.commentMessage}>{this.props.data.message}</span>
   </Col>
                    </Row>
                </Layout>
            </div>
        )
    }
}
export default withRouter(Comment);
