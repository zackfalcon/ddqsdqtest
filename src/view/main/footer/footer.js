import React from 'react';
import { withRouter } from 'react-router-dom';
import { Layout, Row, Col } from 'antd';
import icon from '../../../assets/icon.png';
import Styles from './footer.less';

class Footer extends React.PureComponent {
    render() {
        return (
            <Layout style={{ background: '#f7f7f7' }}>

                {/* <Row span={24} className={Styles.footerBlock}>
                    <Col span={6} offset={6}>
                        <h3><span>À propos</span></h3><br />
                        <span className={Styles.link}>Qui sommes-nous ?</span><br />
                        <span className={Styles.link}>Nos Valeurs</span><br />
                        <span className={Styles.link}>Besoin d'aide  ?</span>
                    </Col>
                    <Col span={6}>
                        <h3><span>Suivez l'aventure</span></h3><br />
                        <span className={Styles.link}>Facebook</span><br />
                        <span className={Styles.link}>Twitter</span><br />
                        <span className={Styles.link}>Linkedin</span>
                    </Col>
                </Row>
                <hr></hr> */}
                <Row className={Styles.footerBlock}>
                    <Col span={12} offset={6} style={{textAlign: 'center'}}>
                            <img alt={"logo"} className={Styles.imgIcon}src={icon}></img>
                        <span className={Styles.devise}>© 2019 ShareXP, l'expérience à porter de main</span>
                    </Col>
                </Row>
            </Layout>
        )
    }
}
export default withRouter(Footer);
