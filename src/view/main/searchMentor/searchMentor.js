import React from 'react';

import Media from "react-media";
import { withRouter } from 'react-router-dom';
import { Layout, Button, Row, Col, Rate, Icon } from 'antd';
import check from '../../../assets/check.png';

import Styles from './searchMentor.less';

const data = [
    {
        name: 'Cecile Garayt-Georges',
        age: 41,
        location: 'La Défense',
        note: 10,
        avis: 5,
        trustedUser: true,
        company: 'https://www.figaret.com/skin/frontend/figaret/default/images/logo.jpg',
        description: 'J\'aime former de nouveaux talents',
        job: 'Directrice de boutique',
        image: 'https://media.licdn.com/dms/image/C4D03AQEInUwUiJfphA/profile-displayphoto-shrink_800_800/0?e=1556150400&v=beta&t=3jatvQgK_TqYXQdmUfyJYeapNS4AIQthIKGU9AuNqIE'
    },
    {
        name: 'Elise Millot',
        age: 47,
        location: 'Neuilly-sur-Seine',
        note: 0,
        avis: 0,
        trustedUser: true,
        company: 'https://civa.qc.ca/wp-content/uploads/2018/09/Accenture-Logo-Purple.png',
        description: 'Toujours à la recherche de nouvelles expériences, je suis très à l\'écoute des autres',
        job: 'Consultante Management et Analyste',
        image: 'https://media.licdn.com/dms/image/C4E03AQH1gg6mqK_sDA/profile-displayphoto-shrink_800_800/0?e=1556150400&v=beta&t=vL7iwyDmv7NnLgZuiOwpYZXlXPDugWBFSexHJLU5LH4'
    },
    {
        name: 'Caroline BODART',
        age: 39,
        location: 'Neuilly-sur-Seine',
        note: 8,
        avis: 1,
        trustedUser: true,
        company: 'https://www.insideboard.com/wp-content/themes/insideboard/theme/images/logos/logo-insideboard_header.png',
        description: 'Accompagne les entreprises vers le succès. Je vise le meilleur',
        job: 'Consultante Marketing et Management',
        image: 'https://media.licdn.com/dms/image/C4E03AQHjtvg09KAUaA/profile-displayphoto-shrink_800_800/0?e=1556150400&v=beta&t=GHfEzA5r0ECosCBOQZVfjN8SPIc6SzXOWlYeeoIqWv0'
    },
    {
        name: 'Eric LACLAVERIE',
        age: 28,
        location: 'La Défense',
        note: 10,
        avis: 10,
        trusedUser: true,
        company: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Soci%C3%A9t%C3%A9_G%C3%A9n%C3%A9rale.svg',
        description: 'Expert dans le domaine de la constultation',
        job: 'Consultant Finances',
        image: 'https://media.licdn.com/dms/image/C5603AQGSWPg7h-ZlOg/profile-displayphoto-shrink_800_800/0?e=1556150400&v=beta&t=BFnYLgPALkQR348_n0DBajG6gRC_nkWvhShbcssv5OE'
    },
    {
        name: 'Martela',
        age: 28,
        location: 'Paris 15éme',
        note: 5,
        avis: 10,
        trustedUser: true,
        company: 'https://www.blendwebmix.com/wp-content/uploads/2018/07/logo-1.png',
        description: `Je suis actuéllement ingénieur en informatique chez swapcard,
        Je suis aussi titulaire d'une licence de mathématiques que j'ai acquise durant ma première année à Centrale Lyon en parallèle à mon parcour`,
        job: 'Ingénieur',
        image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhISEhIVFRUVFRUVFRUXFhUVFRUVFxUWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQFy0dHSAtKystLS0tLS0tKy0tLS0tLS0tLS0tNy0tLS0tKy0tLS0tLS0tLS0tLSstLS03LSstLf/AABEIAOAA4AMBIgACEQEDEQH/xAAcAAABBAMBAAAAAAAAAAAAAAABAAIDBgQFBwj/xABAEAABAwEFBQUGBAQEBwAAAAABAAIDEQQSITFRBQYTQWEicYGRoQcyQlKxwWKS0fAjQ3LxFIKy4TM0U2Nkc6L/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAQIEAwX/xAAjEQEBAAIDAAEEAwEAAAAAAAAAAQIRAyExEgQyQVETYXEi/9oADAMBAAIRAxEAPwDsoCNFDfcjxDoqrDLkk1MeSUQTogmokoLxCN86IJklDeOideOiCSijdmjfOiadUEyRUd86IcQ6IJQiow86I3+iB6RTA86JFxUgsTlG00Sv9FAeiFHxEhJ0QSJJl9G+pAp2lIoS7GqPG6IJUlGJeiN9EHo1UfF6FLi9FIaCihfGqReNVVIEpwKQeNUr41QCXJKPJB7ggx9MEEqVEwSBAyhBIULyj4oQEiCaqYM0L4TC/GqDIJTby1O2t4YLO2r3Y8gMyqfP7SmgkiLDUlR8kzGuil6N9cXtntTke66xtwakVqtjs7fW0VoXA1yNBQqLnpMwtdYqk9U3Ze/DCbsounUZK1Q2xj2gscCDopxylRcbPWSiFGJAjxBqrKnBFM4g1S4gRJ9EHNwQ4g1SdIKIFHkimROwUlQpQSNEg8ao3xqEGKY+iY4dFkKCXMKqUojCN0JBGqAXQlcCKKIMMYS4YTkCiUTG1T+GEyFwqVKXBAAwLXbe2g2CJzyKmmA1PILYSygCpyGa5HvzvOZZuDGcAaCnlVUzy1F8MflVf23bXyPc9xDnnIVwHRap2yJS0ySY86BX3d/dhoaHvFXHGpWTtuwCNoIGtRyIwqFw/k01fxSuU2kCgIy+36hZ2xZSDdrUEVHh90dvWDhPw9x/aYfqFj7PFCOWNR9l23LGfVmSzx9sYHH4Xdflctvu3tqRjiAaFpoWn6f7qs2G2iOajvcce0NDqFv7fZzDJFMPcNGPPf7j1xvVdurHU9jbTZO2owdzH6LZiILntimMEraVuu7TenzNV+ss95oIXbjz364cmHx8PcwI8MJSHBOC6uQCMI3AiEiFIY5gSuDRS0RupoQhg0RawJ5CLMkGJwzqhwieamJQVUojGdUOCdVPRKiDGexw5p0bCfiKmcmQnBAuD1KPC6p9UUQh4I1R4Y1T6IOwCJVTf/aos9ncQcTUDvouc7ibOM0htD8anCvfmp/bNtBz547OzG6KkfidSi2OwrYbEyOOWB926KvaKjrVZ+Rq4YusTKBYe2ILzPTzWVYrbHK28xwI/eanc0HNca7y6c7n2XxmSWc++2skPeMS3xwVNbERhShBOHMEZj0XTtuSwxvY9sjb7TW6DU+IC0m9uyAHttMY7EtC4fK/XuP7zV8LZ1VOTHd2pUzrwB5jA+GRXRN2S212MwvzDbh7vgPofJUa22S64OA7L616HmFY9xZix5aa4OA72uyPgQFfPzbnj+m6bM42TGvEgdjr2TQ+YCu279oDmihzAI7iFVtrQhjpi3KRocRo8HteazN1bTdexn4cPCuCrjdVGU3iu9zUpwZ1RaU4Fa2Uzh9UjH1T6p1VIia06p3DOqbGVKCgjMfVERdU8hCiDEMh0QMp0UpTJeSqkBIdEOIdE8NTrqCMPOicAQn3QjQIhHeOiV86KSg0RACaSh4p0UNrtF1rnEYNBPkKqanaWl37nLLBaXDPhkeeCik9cX2jauNaJ7QcSJG3e8Gv2C37tqWl7Z3i60R3aMIvOcDz6BVfd6zGWWzxfM++7urU+gXVpdkRmnZFdVnt1e23GdddNVupbHvuEsu365dOdNFY7fTAZ0xTdn2QMJPgE+WKpvDmudkdIqtutrIQ+UQm6DQvujM6Vz8FNsHbMNra+CoIcKtBwIPdyWdbdlOkY6JxrG6hu6EaHMLX2XdkR0INCwG7QAU54nmp1JC7t1+GvdsrGSzvwJrdPX4T9lrbKwwlrjh8DujmnmrZNJxRe/mx+8OZA+ILXbw2XiROkYKFw7Q0eOfiolc7NVm2i2iWMP1aQ4aELI2Kx1+Ej8Q+ipm720CQ5hz++qvO7koJYDmCP0+yifcZeLzEDRStBTYXYeCULs1vjCfcKNxGqNVIZcRAKcXJAoG9pND3aKRMY/FBDxmnmmvkrkn3BoiGBVSjvpF6kuhEAIIeKlxVK5qZG3FA2+Ub5Ugaj5IIQTWpC0++MfEsdoZT3o3fSq3tFjbRZejeNQQoynRPXDNzQG2iJ5/p8wQuqNdguaujEVOTmSXvAO/suhtlFK8qArHa9DGaFm1mhpq01GFKY99E+x2prqObiHVwIIIPUFYtmtsNTR7K1qcQthC1nvCmPMfqo7dMsdfhkFozWNazgsi/gsS1pl4ripe3ZHMfxIzdc3mOfes/dvbkE9WS0je4UI/lv6g8jVYO3G4uJVJjtQZIS0kG9Ujljl5gJhjuHLrpZNvbHdZLYDTsSVIPIg459D9VaN3Jv4jBpkedOqGxnMtcAY/tBuI1jdq06FM2XEY53gjBuAPdr5qJe3K+OkQSAjnkkKg5JlifVgPRZIcVvncYr6YH9EanRPDkcVZCG+dE+qB97wUl1A3idCmV6KUBJBHRAUTCwalRujCqlkIFRRxYZlO4I1KA16qNuaeIgjwwgWGoRvDolwghwwmkFfGoUFreLpx5KS5jRQ7RLWxvOgKi+Jnri22amab/ADU86qybJmFpskYvEdm6aGhq3A/RaC3sN6Z55mnmodxtpBkroHe68ksP4ubfFYZ3K9Gdab9m7oFaV7w4/dbKw7Ccz3ZXs7je9Ct5Zg1TyAclMvTtlzZWaY8MZaKF146pk5wTLRaGtzdRaO37YL6shF45F3wjx5+Cq5yK9vltEMBAzP0XPLK9z3l5+J1e4DJWXeiK5eDjVxoSTz5nuC1WzbNVt7Rriu+Gpi5cm7kvvs1tJpniHUI1FVddoWdlS+l3EVcOtKV10VA3AbdcPxEu7l0m1AFl6lcK09f1XG/dUXyNtsZw4YxWfUKtbpvD2PbWpY70IqKrewMBqt2F/wCYx5zVZN4ahK+NVHwxonBvQK6ppeL2JopuK35gmU6BGnQIgeM3VDijVGnRMripSaAmTNwKiErtAiS46KiT4nYKRY4Dhz9EAw6oMlGqxSHfN6JAO+b0QZYSIWIGO+ZO7XzFA+R1CPJYW1I7zHDkB5lZLmE81jzxuoe0VGU2mVy/bUJvlpypgPAmvqFULbZHNaHtq1zHAgjMY1BXS9s7Le4lzc2j0p+tFWLHdnicadppdFK3mHDI9x+6wauNbplMsdMDYO+drfeYWNddIBdWnjRXWI2h7QeI0A6Ak+q5vudZyLTaWHkB9SuobOIuAdFbOTfScLdMV+ymn3i555lxw8skbfJHZ46mgwwHMnQBZ80oa0uOQFVQt4NoGR5ANTjU/CwfK39eapI6K9tqV08jjkBi/Ro5NWbYYw2zveR73ZA7zX9EZLMBG1jM3ntHXH+6zLbDRsMIHO8R+/3guly/Dn8e9tpue8M4JI96QjzFPJdFg9145AnyrgudMaGiIA0DHNFdXXsaK9vkuttBJoBj6A/dcrezKMH2f2g8e1MOQNAdbpcPsrvZ8yqH7PbK4yTSVNHvc8dxAp9VfRCdSF6HF4xcv3MgAI0WPwnfMUuE7Uro5smgSWKYjlU+aQg6nzTYyljEi+iLP1PmkbKOiBgCKTfBEyAcwqpIBKqbxRqE0SBBLeUTswjxBqo3PFQUGUKIqFszdQodo7TihikmkcAyNpe415AVUo0ymnotNtneixWY0ntMTD8pILvyipXDN6PavbrTeZE7/DxGtBHg8t/E/PyoqJxCSXOJJOJJJJPeTmpHoPaHtF2fGx7w90rcuww4k15uphgVSXe0WzSOo2y8IOIBkNC8itBep/uueWaascjCc8R3ihWE0rncJlNVaZ6u46GdpR2W2Okf/wAOZoBcBWhHQcsVY7PvrYQMZx5O/Rcx2jMZII3E4jDxbgfSi0oXPHhmU7d7zXG9Oubb31s8kdyBznXjQm65uApgK6qtRWwyVdS629daBzpgq5Zc8PgaKDV7sB9St3tOPhPghB926T/UaE19VS8cnUdJy29rRsuzuL7zqUYKNHIa/vqpoRWaaU4NjAY2vzZYeKzbJ2Y2YYv/ALkn0QdCHvawCgdIXGnMAZnxWS5d1okQNeL8LKZvBCvW04y5srBhfusr0uip+qo9hHE2hG0e6wF5GnIfZWXe3a9yWCzsrfndy+FobVx71OvHPK7rdbt2+KMiIYVoGuOTqYUCtbHrlm2bUGgtZhw2xjD3mkmod3qXYO/MkVGzu4zagOdQB4BydhnywOq28PJfj2zcuE306heTqrFitF4BzcQcQpA8/KVo2z+Huz8E5qhc8g1oU4z/AISiEtEaKEznk0pold8vqp2I/wDDhEwBPqg4qqxjIwpLjVHAc+9S0RBlGpEN6p9UWoIRE0rk/t625w4obHGaGX+JL/Q00a3xOPguukVXnL242gu2nI2uDI4mj8tT9UkFAqgCgESrBwOCezEpsTK9NTopxO1uDWg9XCtfBRRNPKBEGN5knHnyJHRYAT5HlxqTUpR5qJNJva17vWIOkiqMHODz/S1tfqoxLx7fqC/6FT7rzUjlfzZE8g/1GlEzceOtoZXPE9wB/fksuV18r+mnHvUXO2WgCTPBjg2nhQetVtNlxgvJ5NiJ8XE1+iqkMl6WYE++43e9rqg/UK0buOJjGrmPB76E/dY85qRrn5Ye49nLp7TO4/EI29wzWDtzaxO2YTXBp4Y50qSDTvVi3MutsbnuzfJLJXoC4/Rq5btu2OFrjlGd4P8A/q9RduOfLO/44ZXU2tW2dqOba7YwgijQfykU9PqtBDagZr3Imhpzr/utpvzja+K3+dZ2SU1bdofEKmwT059a9Vs4pPizcmV+TtOwt9zC6zGQ1gma4OPONwe5rXEaUpVdSD64g4cl5tmmrDZgecV78z31BH7zXXvZttsyQcCR1XwtZQnMxuaLteoxHkr4dRTP1dbyNVjGZuqkZMFdQ8lM5p19uqjvUPggjuv6Ihjk68UqlVWMEBrUYFOuu+ZKhRAKBpY75h5KMh2RcpiCo5cwgkDR8y8we1eW9tW2Y1o9rfKNgXp9oC8p+0F97aVtP/ed6AD7KYhXwUihTHuRUh4dhRBIJICiE1EILDu/NSy2zUCPyLjX6Labm4TX/wCked4qvbDk/wCNH/1I/VpqFYtj2cwxtkOZfG49Gg0/VZObqX+2vh7sScU8evO+4DuB+6u+7LwQw9cR31r91Rray7anHkXAt7nYq87pQUa3rSnfeKycvkasfyxJ5THZZIgcbjmjx97xz81zrexl2drdGN/for7K4OntDXZMFB1c6Sn2VG3zINskpk2jPFox+q6/TT/pw59fFvN5Zb9gsNoHvNBiJ5jIkKkvPaP76q2xu4mx3g/yrQ09zXhU95x7qBbOLzTJyXd2tTGl9mssjcS0vgd0xvCv5irt7N9otbtBrAQWyMMQOtwVafG6fNUjdWXiQWuzD3i0TR15uj98Drcr5Kbdy28K0QSHC5KwnuvUP1KtjEV6UlAHinjDkFDaTiFO0q6gF3QI+SJSLggwmWjUFETnSvopARojeGiqsjM34T6ICY53fBS16IFp5H6IBxifh9VFJXP0U4JSJ1CIQB509V5T33/5+1/+9/1XrGN+HivJu97q261H/wAiT/UVMGmupwCc7NAqQkkEUCSSWVs6wyTSNjiaXOccAPqdAot0MrYFjc+VtMAMXHRvOqt0tS0aOa4t7mkXfp6rB2rdsUYs0ZDp30MjvoO4VwW+NiP8BnyxNJP77lh5s99/hv4MNdfljGx8VkTwamIta7u+E+SsdmtfDjj5Fsnpg4fVafY7KSPacpK9wIIDPWvmp94o3R0Iy7J9Fly7umma9pRyfx3P5OuP6YOe5c02paTJNK/5pHu8yui2l92xcTmImnxq/wDVcra7Xqtv02PtYvqcvF03Tk4lk2lCc+EyUDqx+Poqm8EEH+xW33Ltwitcd40ZIHRPr8sjS2vmQsDaURjfJC73o3lvkaA+S0Y9ZWM17xiXY9vME0czRixwdTUZOb3EVHit7tizXJHhhqx/8SI6sf2m+WXgqpG5WzZUv+IhbB/OiqYtXsOJjHUZhWvV2rPNPSMEH8Jl7Ehja99AnMs4pX7oRV4ba53G177oqp4h2R3KyEJs/wC6pws46KUJBqCEFOvt50WOLONSU8WduihJ94fMnFzfmWLLCOQ81LHZ2oHiVuoQfKO9LgNREbRkgED2mtcF5L284G1Wl2Y40tPzuXq61RMDXOIya4+QJXka1vq5x+ZznebifugxygkSgpBCmis73YtYXDoCfooFsNk2gRuLyThlQn7KL4Rstj7sSS0Lw5jTlh2j3A5d5VqtO3rLs2Ew2VrXWhwo53vFvV79egVMt28c7xdaSxpzu+87+p2a1DQuP8eWf3Xr9OszmP2+s6xudLaIy8kufK2pPOrgu1Wiw0e46iNg/wAran6rkO74ZHLHLLgGvaQPica4ADTquz7XtgEwj5lpkb5Nw8ifJZvqvY0fTX1qILHSXHANbeNPErG3uJMUeJN+Fzh4XT9ytvOCWSUwLhdr3jl4fVaHblXQXm0vQQuq3mAXCn+khZsPWjO9NLb7Te2YwjPhXHDkSx4BXPmq4xTB1geBkHPI6BwBA9KKmtXo8E1uf2w8/ev8Oa7Gui3O3bU2e5Pk8tDZBq5ope8QtKnNcfNd9d7cNpGLdbuxl9ogYK1dIwAjq4ZLSMVs9nUN/aFlGkgP5QT9lKHpsMORTY2uGFRT1QbJVOwPJToExnXyUTmPHxHyClupPyTQjDkQ5aszyfPFTx/VSumcMTIwDw/VQll2g4DvUgWmtG1Yxg6YHuCxn7zNGANf8tPumhZQEqKoS7zOyCjdvNKMKjyU6G73wtghsNrl+WCQ+N0gepXk2QrtHtL3jmdYJmF3ZeWNNABm6v2XFC5RZoJBCqFUDwiHJlUQUDqp0Zxzp1UdUqoN1ZNltcQ42hmYJADnv8qLqNotzbkcz3XWtDWOqP4jgMGigrQEYrjlme1pvOxpk3XvK3Ng2+Q8F/uOAbJGMroqBQa0Pos3NxXJ24uSYupQS3pMT2HMLQNCMfPHHuWpjiv2lxc6pdC+GYaYHhvppn+yhJMIoBaA69GKYjG8DQNf5YHqCtNadsNbMyQOANMCfclid8JdyIWTDC76as85pXGh0MFqhfm2QN8sKjwVfCte9DmyMvxnEntDnQZHrTLwVUC38Xc2x8nujkQmVTgurklYr97Ioq25r6e4xx8SLv3VBjXTvZDHQzy6BrfuVMg7Eba4HP0Tm25x5rXSSBwBrQ5Y5FRMlV4htjb3aof400xK1jHpwlyU6H//2Q=='
    },
    {
        name: 'Camille Mear',
        age: 24,
        location: 'Boulogne',
        note: 9,
        avis: 2,
        trustedUser: true,
        company: 'http://flythenest.io/wp-content/uploads/2018/08/logo-thiga-black-2-400x91.png',
        description: 'Je suis toujours à la recherche de nouveaux talents',
        job: 'Directrice de ressources humaines',
        image: 'https://media.licdn.com/dms/image/C5603AQHezGL0PF9MuA/profile-displayphoto-shrink_800_800/0?e=1556150400&v=beta&t=0lmn9VQyN22cp_qfE0d0qT40nJet8BiDQIDlPytcPlA'
    },
    {
        name: 'Bertrand Pelloquin',
        age: 28,
        location: 'France',
        note: 6,
        avis: 3,
        trustedUser: true,
        description: 'Passionné d\'art et de spectacle, je suis un artiste accompli',
        job: 'Ingénieur du son',
        image: 'https://media.licdn.com/dms/image/C5603AQEC_HtuWCZkog/profile-displayphoto-shrink_800_800/0?e=1556150400&v=beta&t=I_LsJKnaXxTQVvlrc6BY2GifEvMQ2IB5Z4GX5iynOGg'
    },
    {
        name: 'Ines Brahim',
        age: 31,
        location: 'Paris',
        note: 9,
        avis: 2,
        trustedUser: true,
        company: '',
        description: 'Passionnée d\'architecture commerciale, j\'aime partager ma passion et mon expérience.',
        job: 'Architecte',
        image: 'https://media.licdn.com/dms/image/C5603AQHB7qGCrQYiLQ/profile-displayphoto-shrink_800_800/0?e=1556150400&v=beta&t=H2BbNGHCUSOBbcbGnvJsNOFSXFPWc6yPjMYJUBJVh8I'
    },
    {
        name: 'Monica Boi',
        age: 34,
        location: 'Seine-Saint-Denis',
        note: 8,
        avis: 1,
        trustedUser: true,
        company: '',
        description: 'Je pense que le futur va se construire avec les nouvelles générations, c\'est à nous de les former',
        job: 'Assistante de direction',
        image: 'https://media.licdn.com/dms/image/C4D03AQFBTu-ggvzTsg/profile-displayphoto-shrink_800_800/0?e=1556150400&v=beta&t=QX8uQ4rL21jU8YTo02VwuOU36uz4WwEBKTadYZKX6KQ'
    },
];

class SearchMentor extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            ...this.props.location.state,
            id: 0
        }
        this.onProfileMentor = this.onProfileMentor.bind(this);
    }

    componentDidMount() {
        console.log("Mounting")
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.job !== nextProps.location.state.job) {
            this.setState({ job: nextProps.location.state.job })
            return true;
        }
        if (this.state.location !== nextProps.location.state.location) {
            this.setState({ location: nextProps.location.state.location })
            return true;
        }
        return false;
    }

    onProfileMentor(id) {
        this.setState({
            error: null,
        })

        this.setState({
            onSearch: true
        })
        this.props.history.push({
            pathname: '/profilMentor',
            state: {
                data: data[id]
            }
        })
    }

    render() {
        return (
            <Layout style={{ background: '#f7f7f7' }}>
                <Row className={Styles.blockGridList}>
                    <Col span={15} offset={5} className={Styles.colResize}>
                        {data.map((mentors, index) =>
                            <Media query={{ maxWidth: 777 }}>{
                                matches => matches ? (
                                    <Row gutter={20} className={Styles.gridListByUser}>
                                        <Row gutter={20}>
                                            <Col span={10} className={Styles.imgUser}><img alt="" src={mentors.image} width="250" /></Col>
                                            <Col span={10} className={Styles.btnContainer}>
                                                <Button type="primary" className={Styles.buttonMentor} onClick={() => this.onProfileMentor(index)}>
                                                    <div className={Styles.textBtn}>Réserver</div>
                                                    <img src={check} alt="logo" className={Styles.checkoutBtn} />
                                                </Button>
                                            </Col>
                                        </Row>
                                        <Row gutter={20} className={Styles.userInformation}>
                                            <h1 className={Styles.userName}><span className={Styles.colorized}>{mentors.name},</span> {mentors.age} ans</h1>
                                            <h2 className={Styles.userJob}><span className={Styles.colorized}>{this.state.job},</span> à {this.state.location}</h2>
                                            <h2 className={Styles.userCompany}>SOCIETE: <img className={Styles.imgJob} alt="" src={mentors.company} width="120px" /></h2>
                                            <div style={{ display: 'block' }}>
                                                <Rate style={{ color: '#00C9FF', display: 'inline-block', marginBottom: 10 }} disabled defaultValue={mentors.note} />
                                                <span style={{ display: 'inline-block', fontWeight: 'bold', color: '#444444' }}>({mentors.avis} avis)</span>
                                            </div>
                                            <hr style={{ opacity: 0.3 }} />
                                            <span className={Styles.userDesc}>{mentors.description}</span>
                                        </Row>
                                    </Row>
                                ) : (
                                        <Row gutter={20} className={Styles.gridListByUser}>
                                            <Col span={6} className={Styles.imgUser}><img alt="" src={mentors.image} width="250" /></Col>
                                            <Col span={12} className={Styles.userInformation}>
                                                <h1 className={Styles.userName}><span className={Styles.colorized}>{mentors.name},</span> {mentors.age} ans</h1>
                                                <h2 className={Styles.userJob}><span className={Styles.colorized}>{this.state.job},</span> à {this.state.location}</h2>
                                                <h2 className={Styles.userCompany}>SOCIETE: <img className={Styles.imgJob} alt="" src={mentors.company} width="120px" /></h2>
                                                <div style={{ display: 'block' }}>
                                                    <Rate style={{ color: '#00C9FF', display: 'inline-block', marginBottom: 10 }} disabled defaultValue={mentors.note} />
                                                    <span style={{ display: 'inline-block', fontWeight: 'bold', color: '#444444' }}>({mentors.avis} avis)</span>
                                                </div>
                                                <hr style={{ opacity: 0.3 }} />
                                                <span className={Styles.userDesc}>{mentors.description}</span>
                                            </Col>
                                            <Col span={5} >
                                                <Button type="primary" className={Styles.buttonMentor} onClick={() => this.onProfileMentor(index)}>
                                                    <div className={Styles.textBtn}>
                                                        <Media query={{ maxWidth: 1260 }}>{
                                                            matches => matches ? (<p>Réserver</p>) : (<p><Icon className={Styles.profil} type="user" />Voir Profil</p>)}
                                                        </Media>
                                                    </div>
                                                    <img src={check} alt="logo" className={Styles.checkoutBtn} />
                                                </Button>
                                            </Col>
                                        </Row>
                                    )}
                            </Media>
                        )}
                    </Col>
                </Row>
            </Layout>
        );
    }
}

export default withRouter(SearchMentor);
