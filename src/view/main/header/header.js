import React from 'react';
import logo from '../../../assets/logo.png';
import { withRouter, Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import Styles from './header.less';

class Header extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			isLogged: false,
			name: ''
		}
		this.registerRedirect = this.registerRedirect.bind(this);
		this.loginRedirect = this.loginRedirect.bind(this);
		this.logoutRedirect = this.logoutRedirect.bind(this);
		this.mentorRedirect = this.mentorRedirect.bind(this);
	}

	registerRedirect() {
		this.props.history.push('/auth/register');
	}

	loginRedirect() {
		this.props.history.push('/auth/login');
	}

	mentorRedirect() {
		this.props.history.push('/auth/mentorLogin');
	}

	logoutRedirect() {
		localStorage.clear();
		this.props.history.push('/home');
		window.location.reload();
	}

	componentDidMount() {
		if (!localStorage.getItem('jwtToken')) {
			this.setState({
				isLogged: false
			})
		} else {
			this.setState({
				isLogged: true
			})
			const name = localStorage.getItem('userName');
			if (name) {
				this.setState({
					name: name
				})
			}
		}
	}
	render() {
		return (
			<Layout className={Styles.headerBlock} >
				<Menu
					theme="light"
					mode="horizontal"
					selectedKeys={"mentor"}
					className={Styles.menuBlock}>
					<Link to="/home"><img src={logo} alt="logo" className={Styles.headerLogo} /></Link>
					{this.state.isLogged && <Menu.Item onClick={this.logoutRedirect} key="logout">Déconnexion</Menu.Item>}
					{this.state.isLogged === false && <Menu.Item key="mentor" className={Styles.becomeMentor} onClick={this.mentorRedirect}>Devenir Mentor</Menu.Item>}
					<Menu.Item key="help">Aide</Menu.Item>
					{this.state.isLogged === false && <Menu.Item className={Styles.loginButton} onClick={this.loginRedirect} key="login">Se connecter</Menu.Item>}
					{this.state.isLogged === false && <Menu.Item onClick={this.registerRedirect} key="register">S'inscrire</Menu.Item>}
					{this.state.isLogged && <Menu.Item key="username" className={Styles.userName}>Hello {this.state.name}</Menu.Item>}
				</Menu>
			</Layout>
		);
	}
}

export default withRouter(Header)
