import React from 'react';
import { Button, Icon } from 'antd';
import Styles from './linkedinConnect.less';

class LinkedinConnect extends React.PureComponent {
    render() {
        return (
            <div style={{marginTop: 20}}>
                <Button className={Styles.linkedinButton}><Icon type="linkedin" style={{fontSize: 17}}theme="filled" /> Linkedin Connect</Button>
            </div>
        )
    }
}

export default LinkedinConnect;