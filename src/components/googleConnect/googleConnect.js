import React from 'react';
import { Button, Icon } from 'antd';
import Styles from './googleConnect.less';

class GoogleConnect extends React.PureComponent {
    render() {
        return (
            <div style={{marginTop: 20}}>
                <Button className={Styles.googleButton}><Icon type="google" style={{fontSize: 17}}/> Google Connect</Button>
            </div>
        )
    }
}

export default GoogleConnect;