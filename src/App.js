import React, { Component } from 'react';
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { LocaleProvider } from 'antd';
import frFR from 'antd/lib/locale-provider/fr_FR';
import AuthLayout from './view/auth/authLayout';
import LoginLayout from './view/auth/login';
import RegisterLayout from './view/auth/register';
import MainLayout from './view/main/mainLayout';
import MentorLogin from './view/auth/mentorLogin';
import MentorProfile from './view/auth/mentorProfile';

import createBrowserHistory from "history/createBrowserHistory";
import profilMentor from './view/main/profile/profileMentor';
import bookingConfirmed from './view/main/bookingConfirmed';

import './App.css';

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql"
});

const history = createBrowserHistory()

class App extends Component {
  render() {
    return (
      <LocaleProvider locale={frFR}>
        <ApolloProvider client={client}>
          <Router history={history}>
            <Switch>
              <Route exact path="/auth" component={AuthLayout} />
              <Route exact path="/auth/login" component={LoginLayout} />
              <Route exact path="/auth/register" component={RegisterLayout} />
              <Route exact path="/auth/mentorLogin" component={MentorLogin} />
              <Route exact path="/auth/mentorProfile" component={MentorProfile} />
              <Route exact path="/profilMentor" component={profilMentor} />
              <Route exact path="/bookingConfirmed" component={bookingConfirmed} />
              <Route path="/" component={MainLayout} />
            </Switch>
          </Router>
        </ApolloProvider>
      </LocaleProvider>
    );
  }
}

export default App;
