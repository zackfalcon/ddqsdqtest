# base image
FROM node:9.6.1

RUN mkdir /usr/src/sharexp_frontend
WORKDIR /usr/src/sharexp_frontend

ENV PATH /usr/src/sharexp_frontend

ENV PATH /usr/src/sharexp_frontend/node_modules/.bin:$PATH

COPY package.json /usr/src/sharexp_frontend/package.json

EXPOSE 1000

RUN npm install

CMD npm start